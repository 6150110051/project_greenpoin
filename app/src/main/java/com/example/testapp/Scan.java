package com.example.testapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.google.zxing.Result;

import me.dm7.barcodescanner.zxing.ZXingScannerView;

public class Scan extends AppCompatActivity implements ZXingScannerView.ResultHandler {

    private ZXingScannerView mScannerView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mScannerView = new ZXingScannerView(this);
        setContentView(mScannerView);
    }
    @Override
    public void onResume() {
        super.onResume();
        mScannerView.setResultHandler(this); // Register ourselves as a handler for scan results.
        mScannerView.startCamera(); // Start camera on resume
        mScannerView.setFlash(false);
        mScannerView.setAutoFocus(true);
    }
    @Override
    public void onPause() {
        super.onPause();
        mScannerView.stopCamera(); // Stop camera on pause
    }
    @Override
    public void handleResult(Result rawResult) {
        // Do something with the result here
        Log.v("QRcodeScanner", rawResult.getText()); // Prints scan results
        Log.v("QRcodeScanner", rawResult.getBarcodeFormat().toString()); // Prints the scan format(qrcode, pdf417 etc.)
        Intent intent = new Intent(getApplicationContext(),AddPoint.class);
        intent.putExtra("QRcode",rawResult.getText());
        startActivity(intent);
        onBackPressed();
        // If you would like to resume scanning, call this method below:
        /*mScannerView.resumeCameraPreview(this);*/

    }
}
