package com.example.testapp;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class ViewPromotion extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_promotion);

        int[] resId = { R.drawable.b0
                , R.drawable.b8, R.drawable.b1
                , R.drawable.b2, R.drawable.b3
                , R.drawable.b4, R.drawable.b5
                , R.drawable.b7, R.drawable.b6 };
        String[] list = { "ลดกระจาย ปกติ10แต้ม1แก้ว ลดเหลือ5แต้ม!! ", "ลดกระจาย ปกติ40แต้มแลกกระเป๋าผ้า1ใบ ลดเหลือ30แต้ม!!"
                , "ลดกระจาย ปกติ50แต้มแลกกล่องข้าว1กล่อง ลดเหลือ35แต้ม!!", "โปรสุดคุ้ม 1แถม1 ผลิตภัณฑ์ซักผ้า"
                , "โปรสุดคุ้ม ลด50% ผลิตภัณฑ์ยาสระผม", "โปรสุดคุ้ม ลด30% ผลิตภัณฑ์อาบน้ำ"
                , "โปรสุดคุ้ม ลด60% ผลิตภัณฑ์บำรุงผิว", "โปรสุดคุ้ม ลด30% ผลิตภัณฑ์เครื่องสำอาง" , "โปรสุดคุ้ม ลด30% ขนม ขบเคี้ยว" };

        CustomAdapter adapter = new CustomAdapter(getApplicationContext(), list, resId);
        ListView listView = (ListView)findViewById(R.id.listView);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                TextView text = (TextView) view.findViewById(R.id.textView1);
                String cartoon = text.getText().toString();
                Toast.makeText(getApplicationContext(), cartoon, Toast.LENGTH_SHORT).show();
            }
        });
    }
}