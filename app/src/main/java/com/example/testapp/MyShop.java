package com.example.testapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class MyShop extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_shop);
    }

    public void MyPoint(View view) {
        Intent intent = new Intent(getApplicationContext(),MyPoint.class);
        startActivity(intent);
    }

    public void ViewReward(View view) {
        Intent intent = new Intent(getApplicationContext(),ViewReward.class);
        startActivity(intent);
    }

    public void Promotion(View view) {
        Intent intent = new Intent(getApplicationContext(),Promotion.class);
        startActivity(intent);
    }

    public void exchangerewards(View view) {
        Intent intent = new Intent(getApplicationContext(),ExchangReward.class);
        startActivity(intent);
    }

    public void Profile(View view) {
        Intent intent = new Intent(getApplicationContext(),Profile.class);
        startActivity(intent);
    }

    public void Home(View view) {
        Intent intent = new Intent(getApplicationContext(),HomeApp.class);
        startActivity(intent);
    }
}