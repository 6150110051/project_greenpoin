package com.example.testapp;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class ViewReward extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_reward);

        int[] resId = { R.drawable.r1
                , R.drawable.r1, R.drawable.r1
                , R.drawable.r1, R.drawable.r1
                , R.drawable.r2, R.drawable.r3
                , R.drawable.r8, R.drawable.r7
                , R.drawable.r4, R.drawable.r6 };
        String[] list = { "Advertising 1 Day", "Advertising 7 Day"
                , "Advertising 30 Day", "Advertising 1 Month", "Advertising 3 Month"
                , "พัดลม", "ไมโครเวฟ", "ตู้เย็น" , "โทรทัศน์", "โทรศัพท์", "รถมอเตอร์ไซค์" };

        CustomAdapter adapter = new CustomAdapter(getApplicationContext(), list, resId);
        ListView listView = (ListView)findViewById(R.id.listView);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                TextView text = (TextView) view.findViewById(R.id.textView1);
                String cartoon = text.getText().toString();
                Toast.makeText(getApplicationContext(), cartoon, Toast.LENGTH_SHORT).show();
            }
        });
    }
}