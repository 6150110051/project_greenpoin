package com.example.testapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import static android.Manifest.permission.CAMERA;

public class AddPoint extends AppCompatActivity {

    private static final int REQUEST_CAMERA = 100;
    private static String[] PERMISSIONS_CAMERA = {CAMERA};

    public static TextView idCustomer;

    Double money,value = 0.0 ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_point);

        final EditText editText = findViewById(R.id.Amount);
        editText.getText().toString();

        Button btn1 = findViewById(R.id.CheckPoint);

        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                money = Double.parseDouble(editText.getText().toString());
                value = money / 2.5;
                TextView txt__p = findViewById(R.id.Point);
                txt__p.setText(value.toString());
                Toast.makeText(getApplicationContext(), String.valueOf(value), Toast.LENGTH_SHORT).show();

            }
        });

        idCustomer = (TextView) findViewById(R.id.idCustomer);

        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        if (currentapiVersion >= android.os.Build.VERSION_CODES.M) {
            if (checkPermission()) {
                Toast.makeText(getApplicationContext(), "Permission already granted",
                        Toast.LENGTH_LONG).show();
            } else {
                requestPermission(AddPoint.this);
            }
        }
        idCustomer = (TextView) findViewById(R.id.idCustomer);
        Intent intent = this.getIntent();
        String result = intent.getExtras().getString("QRcode");
        idCustomer.setText((result));

    }


    private boolean checkPermission() {
        // Check if we have permission
        return (ContextCompat.checkSelfPermission(
                getApplicationContext(),
                CAMERA) == PackageManager.PERMISSION_GRANTED
        );
    }

    private void requestPermission(Activity activity) {
        // We don't have permission so prompt the user
        ActivityCompat.requestPermissions(
                activity,
                PERMISSIONS_CAMERA,
                REQUEST_CAMERA
        );
    }

    public void onRequestPermissionsResult(int requestCode, String permissions[],
                                           int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CAMERA:
                if (grantResults.length > 0) {
                    boolean cameraAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    if (cameraAccepted) {
                        Toast.makeText(getApplicationContext(), "Permission Granted, Now you can access camera", Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(getApplicationContext(), "Permission Denied, You cannot access and camera", Toast.LENGTH_LONG).show();
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            if (shouldShowRequestPermissionRationale(CAMERA)) {
                                showMessageOKCancel("You need to allow access to the permissions",
                                        new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                    requestPermissions(new
                                                                    String[]{CAMERA},
                                                            REQUEST_CAMERA);
                                                }
                                            }
                                        });
                                return;
                            }
                        }
                    }
                }
                break;
        }
    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener
            okListener) {
        new AlertDialog.Builder(AddPoint.this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }

    public void ClickAddPoint(View view) {
        Intent intent = new Intent(this, MyCustomer.class);
        startActivity(intent);
    }


}