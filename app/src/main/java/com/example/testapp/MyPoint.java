package com.example.testapp;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class MyPoint extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_point);
        int[] resId = { R.drawable.coin
                , R.drawable.coin, R.drawable.coin
                , R.drawable.coin, R.drawable.coin
                , R.drawable.coin, R.drawable.coin
                , R.drawable.coin, R.drawable.coin
                , R.drawable.coin, R.drawable.coin };
        String[] list = { " +50 Point 12 - 01 - 2021 ", " +12 Point 15 - 01 - 2021"
                , " -20 Point 15 - 01 - 2021 ", " +15 Point 16 - 01 - 2021 ", " +45 Point 16 - 01 - 2021 "
                , " +55 Point 17 - 01 - 2021 ", " -50 Point 17 - 01 - 2021 "," +18 Point 18 - 01 - 2021 "
                , " +50 Point 18 - 01 - 2021 ", " -5 Point 19 - 01 - 2021 ", " +53 Point 20 - 01 - 2021 "};
        CustomAdapter adapter = new CustomAdapter(getApplicationContext(), list, resId);
        ListView listView = (ListView)findViewById(R.id.listView);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                TextView text = (TextView) view.findViewById(R.id.textView1);
                String cartoon = text.getText().toString();
                Toast.makeText(getApplicationContext(), cartoon, Toast.LENGTH_SHORT).show();
            }
        });
    }
}