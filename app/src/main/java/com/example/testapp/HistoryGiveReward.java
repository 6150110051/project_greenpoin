package com.example.testapp;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class HistoryGiveReward extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history_give_reward);

        int[] resId = {R.drawable.pillowblanket, R.drawable.neckpillow, R.drawable.b1,
                       R.drawable.dish, R.drawable.b0, R.drawable.b8};
        String[] list = {"หมอนผ้าห่ม", "หมอนรองคอ", "กล่องข้าว", "จาน", "แก้ว", "ถุงผ้า"};
        CustomAdapter adapter = new CustomAdapter(getApplicationContext(), list, resId);
        ListView listView = (ListView) findViewById(R.id.listView);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                TextView text = (TextView) view.findViewById(R.id.textView1);
                String cartoon = text.getText().toString();
                Toast.makeText(getApplicationContext(), cartoon, Toast.LENGTH_SHORT).show();
            }
        });
    }
}