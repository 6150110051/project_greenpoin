package com.example.testapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class Promotion extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_promotion);
    }
    public void ViewPromotion(View view) {
        Intent intent = new Intent(getApplicationContext(),ViewPromotion.class);
        startActivity(intent);
    }

    public void AddPromotion(View view) {
        Intent intent = new Intent(getApplicationContext(),AddPromotion.class);
        startActivity(intent);
    }
}